package me.elkady.muinterview.viewmodels;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;

import me.elkady.muinterview.MUApp;
import me.elkady.muinterview.R;
import me.elkady.muinterview.datarepos.BookingService;
import me.elkady.muinterview.models.Appointments;
import me.elkady.muinterview.views.BookNewActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends BaseObservable {
    public ObservableField<String> userId;
    public ObservableInt progressVisibility;
    public ObservableBoolean loginBtnAllowed;
    public ObservableField<String> errorMessage;

    private Context context;

    public LoginViewModel(Context context) {
        this.context = context;
        userId = new ObservableField<>();
        loginBtnAllowed = new ObservableBoolean(true);
        progressVisibility = new ObservableInt(View.GONE);
        errorMessage = new ObservableField<>(null);
    }

    private boolean validateLogin() {
        errorMessage.set(null);
        try {
            int iUserId = Integer.parseInt(userId.get());
            return iUserId > 0 && iUserId <= 100;
        } catch (Exception e) {
            return false;
        }
    }

    public void login() {
        if (validateLogin()) {
            int iUserId = Integer.parseInt(userId.get());
            BookingService.getBookingApi().getMyAppointments(iUserId).enqueue(new Callback<Appointments>() {
                @Override
                public void onResponse(Call<Appointments> call, Response<Appointments> response) {
                    loginBtnAllowed.set(true);
                    progressVisibility.set(View.GONE);

                    context.startActivity(new Intent(context, BookNewActivity.class));
                }

                @Override
                public void onFailure(Call<Appointments> call, Throwable t) {
                    Log.e(MUApp.LOG_TAG, "Error getting Appointments", t);
                    loginBtnAllowed.set(true);
                    progressVisibility.set(View.GONE);
                }
            });

            loginBtnAllowed.set(false);
            progressVisibility.set(View.VISIBLE);
        } else {
            errorMessage.set(context.getString(R.string.login_invalid_userid));
        }
    }

    public void onDestroy(){

    }
}
