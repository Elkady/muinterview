package me.elkady.muinterview.viewmodels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;

import me.elkady.muinterview.MUApp;
import me.elkady.muinterview.R;
import me.elkady.muinterview.datarepos.BookingService;
import me.elkady.muinterview.models.Appointments;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MAK on 4/19/17.
 */

public class BookNewViewModel extends BaseObservable {
    public ObservableField<String> noHours;
    public ObservableInt progressVisibility;
    public ObservableBoolean nextBtnAllowed;
    public ObservableField<String> errorMessage;
    private Context context;

    public BookNewViewModel(Context context) {
        this.context = context;
        noHours = new ObservableField<>();
        nextBtnAllowed = new ObservableBoolean(true);
        progressVisibility = new ObservableInt(View.GONE);
        errorMessage = new ObservableField<>(null);
    }

    private boolean validateNoHours() {
        errorMessage.set(null);
        try {
            int inoHours = Integer.parseInt(noHours.get());
            return inoHours >= 1 && inoHours <= 8;
        } catch (Exception e) {
            return false;
        }
    }

    public void findAvailableTimes() {
        if (validateNoHours()) {
            int inoHours = Integer.parseInt(noHours.get());
            BookingService.getBookingApi().getAvailabledates(inoHours).enqueue(new Callback<Appointments>() {
                @Override
                public void onResponse(Call<Appointments> call, Response<Appointments> response) {
                    nextBtnAllowed.set(true);


                }

                @Override
                public void onFailure(Call<Appointments> call, Throwable t) {
                    Log.e(MUApp.LOG_TAG, "Error getting Appointments", t);
                    nextBtnAllowed.set(true);
                    progressVisibility.set(View.GONE);
                }
            });
            nextBtnAllowed.set(false);
            progressVisibility.set(View.VISIBLE);
        } else {
            errorMessage.set(context.getString(R.string.booknew_invalid_nohours));
        }
    }

    @BindingAdapter("app:errorText")
    public static void setTextInputLayoutErrorMessage(TextInputLayout view, String message) {
        view.setError(message);
    }
}
