package me.elkady.muinterview.datarepos;

import android.net.Uri;

/**
 * Created by MAK on 4/19/17.
 */

public class MUContract {
    public static final String CONTENT_AUTHORITY = "me.elkady.muinterview";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_APPOINTMENT = "appointment";
}
