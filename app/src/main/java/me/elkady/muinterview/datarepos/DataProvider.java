package me.elkady.muinterview.datarepos;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import me.elkady.muinterview.MUApp;
import me.elkady.muinterview.models.Appointments;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MAK on 4/19/17.
 */

public class DataProvider extends ContentProvider {
    private static final int CODE_1 = 1000;
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = MUContract.CONTENT_AUTHORITY;
        matcher.addURI(authority, MUContract.PATH_APPOINTMENT + "/#", CODE_1);
        return null;
    }


    @Override
    public boolean onCreate() {
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        if(sUriMatcher.match(uri) == CODE_1) {
            // TODO check in DB first, if not there then log from backend then save in DB
        }
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    public interface OnDataLoaded {
        public void onMyAppointmentsLoaded(Appointments appointments);
        public void onMyAppointmentsLoadingError(Throwable t);
    }
}
