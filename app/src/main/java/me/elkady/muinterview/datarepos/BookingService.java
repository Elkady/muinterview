package me.elkady.muinterview.datarepos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import io.reactivex.Observable;
import me.elkady.muinterview.BuildConfig;
import me.elkady.muinterview.models.Appointment;
import me.elkady.muinterview.models.Appointments;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by MAK on 4/19/17.
 */

public class BookingService {
    private static BookingApi bookingApi;
    public static BookingApi getBookingApi(){
        if(bookingApi== null) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Appointment.class, new Appointment.AppointmentDeserializer());
            Gson myGson = gsonBuilder.create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(myGson))
                    .build();

            bookingApi = retrofit.create(BookingApi.class);
        }
        return bookingApi;
    }


    public interface BookingApi {
        @GET("user/{id}/appointments")
        Call<Appointments> getMyAppointments(@Path("id") int userId);

        @GET("availabledates")
        Call<Appointments> getAvailabledates(@Query("hours") int hours);

        @GET("availabledates")
        Call<Appointments> getAvailabledates(@Query("hours") int hours, @Query("date") String date);

        @GET("booking")
        Call<List<Appointment>> createAppointment(@Path("id") int userId, @Query("hours") int hours, @Query("date") String date);
    }
}
