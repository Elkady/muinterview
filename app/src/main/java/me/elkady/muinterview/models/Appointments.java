package me.elkady.muinterview.models;

import java.util.List;

/**
 * Created by MAK on 4/19/17.
 */

public class Appointments {
    private List<Appointment> appointments;


    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }
}
