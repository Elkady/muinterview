package me.elkady.muinterview.models;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by MAK on 4/19/17.
 */

public class Appointment {
    private String date;
    private String time;
    private boolean booked;
    private boolean selected;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public static class AppointmentDeserializer implements JsonDeserializer<Appointment> {
        @Override
        public Appointment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new Gson();
            String res = gson.fromJson(json, String.class);

            Appointment a = new Appointment();
            if (res.indexOf("T") > 0) {

            } else {
                a.setDate(res);
            }
            return a;
        }
    }
}
