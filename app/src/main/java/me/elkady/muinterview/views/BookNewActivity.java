package me.elkady.muinterview.views;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.elkady.muinterview.R;
import me.elkady.muinterview.databinding.ActivityBookNewBinding;
import me.elkady.muinterview.viewmodels.BookNewViewModel;

public class BookNewActivity extends AppCompatActivity {
    private ActivityBookNewBinding binding;
    private BookNewViewModel bookNewViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_book_new);
        bookNewViewModel = new BookNewViewModel(this);
        binding.setBookVM(bookNewViewModel);
        setSupportActionBar(binding.toolbar);
        binding.toolbar.setTitle(R.string.login_activity_name);
        setTitle(R.string.login_activity_name);
    }
}
