package me.elkady.muinterview.views;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.elkady.muinterview.R;
import me.elkady.muinterview.databinding.ActivityLoginBinding;
import me.elkady.muinterview.viewmodels.LoginViewModel;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;
    private LoginViewModel loginviewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginviewModel = new LoginViewModel(this);
        binding.setLoginVM(loginviewModel);
        setSupportActionBar(binding.toolbar);
        binding.toolbar.setTitle(R.string.login_activity_name);
        setTitle(R.string.login_activity_name);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginviewModel.onDestroy();
    }
}
